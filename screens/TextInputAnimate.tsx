import * as React from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
  UIManager,
  Animated,
  Easing,
  TextInput,
} from "react-native";

function TextInputAnimateScreen() {
  const textInput = React.useRef<TextInput>(null);
  const width = React.useRef(new Animated.Value(200)).current;
  const animateWidth = (value: number) => {
    Animated.timing(width, {
      toValue: value,
      duration: 300,
      useNativeDriver: false,
      easing: Easing.inOut(Easing.ease),
    }).start();
  };
  const onFocus = () => {
    animateWidth(400);
  };
  const onBlur = () => {
    animateWidth(200);
  };
  const onSubmit = () => {
    textInput.current?.blur();
  };
  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <Animated.View style={{ width: width }}>
        <TextInput
          ref={textInput}
          onFocus={onFocus}
          onBlur={onBlur}
          style={{ height: 50, backgroundColor: "orange", margin: 20 }}
        ></TextInput>
      </Animated.View>
      <Button title="Toggle input" onPress={onSubmit}></Button>
    </View>
  );
}

const styles = StyleSheet.create({});

export default TextInputAnimateScreen;
