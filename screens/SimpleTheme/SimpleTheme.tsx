import React, { useState } from 'react';
import { View, Text, Button } from 'react-native';
import getStyleSheet from './styles';

function SimpleThemeScreen() {
    const [darkTheme, setDarkTheme] = useState(false);
    const styles = getStyleSheet(darkTheme);
    return (
      <View style={styles.container}>
        <View style={styles.box}>
            <Button title="Cliccami" onPress={() => setDarkTheme(!darkTheme)}></Button>
        </View>
      </View>
    );
}

// interface State {
//     darkTheme: boolean
// };

// class SimpleThemeScreen2 extends React.Component<{}, State> {

//     constructor(props) {
//         super(props);
//         this.state.darkTheme = false;
//     }

//     state = {
//         darkTheme: false
//     }

//     toggle = () => {
//         this.setState({darkTheme: !this.state.darkTheme});
//     }

//     render() {
//         const styles = getStyleSheet(this.state.darkTheme);
//         return (
//             <View style={styles.container}>
//               <View style={styles.box}>
//                   <Button title="Cliccami" onPress={this.toggle}></Button>
//               </View>
//             </View>
//           );
//     }
// }

export default SimpleThemeScreen;