import { StyleSheet, ViewStyle } from 'react-native';

export const Colors = {
    dark: 'black',
    light: 'white'
};

const baseContainerStyle: ViewStyle = { 
    flex: 1, 
    flexDirection: 'column',
    alignItems: 'center', 
    justifyContent: 'center',
};

const baseBoxStyle: ViewStyle = {
    borderWidth: 2,
    width: 150,
    height: 150,
    alignItems: 'center', 
    justifyContent: 'center',
};

type Style = {
    container: ViewStyle;
    box: ViewStyle;
}

const lightStyleSheet = StyleSheet.create<Style>({
    container: {
        ...baseContainerStyle,
        backgroundColor: Colors.light
    },
    box: {
        ...baseBoxStyle,
        borderColor: Colors.dark
    }
});

const darkStyleSheet = StyleSheet.create<Style>({
    container: {
        ...baseContainerStyle,
        backgroundColor: Colors.dark
    },
    box: {
        ...baseBoxStyle,
        borderColor: Colors.light
    }
});

const getStyleSheet = (userDarkTheme: boolean): Style => userDarkTheme ? darkStyleSheet : lightStyleSheet;

export default getStyleSheet;