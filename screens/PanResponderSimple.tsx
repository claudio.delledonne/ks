import * as React from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  PanResponder,
  GestureResponderEvent,
  PanResponderGestureState,
} from "react-native";

const { width, height } = Dimensions.get("window");
const initialPosition = { x: width / 2 - 100, y: height / 2 - 100 };

const PanResponderSimpleScreen = () => {
  // const [oPosition, setOPosition] = React.useState(initialPosition);
  const [position, setPosition] = React.useState(initialPosition);

  const handlePanResponderMove = (
    evt: GestureResponderEvent,
    gestureState: PanResponderGestureState
  ) => {
    console.log(gestureState);
    // let xdiff = gestureState.x0 - gestureState.moveX;
    // let ydiff = gestureState.y0 - gestureState.moveY;
    // try with dx and dy
    setPosition({
      x: position.x + gestureState.dx,
      y: position.y + gestureState.dy,
    });
  };

  const handlePanResponderRelease = (
    evt: GestureResponderEvent,
    gestureState: PanResponderGestureState
  ) => {
    setPosition(initialPosition);
    // setOPosition(initialPosition);
  };

  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => console.log(gestureState),
      onPanResponderMove: handlePanResponderMove,
      onPanResponderRelease: handlePanResponderRelease,
    })
  ).current;

  return (
    <View style={{ flex: 1 }}>
      <Text style={styles.text}>
        x: {position.x} y: {position.y}
      </Text>
      <View
        {...panResponder.panHandlers}
        style={[
          styles.box,
          {
            marginLeft: position.x,
            marginTop: position.y,
          },
        ]}
      ></View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    position: "absolute",
    textAlign: "center",
    marginTop: 50,
    width,
  },
  box: {
    position: "absolute",
    height: 200,
    width: 200,
    backgroundColor: "red",
    borderRadius: 5,
  },
});

export default PanResponderSimpleScreen;
