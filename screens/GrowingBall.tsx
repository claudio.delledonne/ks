import React from 'react'
import {Button, StyleSheet, Text, View} from 'react-native'
import Animated, {
    useAnimatedGestureHandler,
    useAnimatedStyle,
    useSharedValue,
    withSpring
} from 'react-native-reanimated'
import {TapGestureHandler, TapGestureHandlerGestureEvent} from "react-native-gesture-handler";

const GrowingBall
    = (): JSX.Element => {
    const pressed = useSharedValue(false)
    const boxAnimatedStyle = useAnimatedStyle(() => {
        return {
            backgroundColor: pressed.value ? 'red' : 'coral',
            transform: [{scale: withSpring(pressed.value ? 1.5 : 1, {velocity: 2})}]
        }
    })
    const onTap = useAnimatedGestureHandler<TapGestureHandlerGestureEvent>({
        onStart: (event, context) => {
            pressed.value = true
        },
        onEnd: (event, context) => {
            pressed.value = false
        }
    })
    return (
        <View style={styles.container}>
            <TapGestureHandler onGestureEvent={onTap}>
                <Animated.View style={[styles.box, boxAnimatedStyle]}/>
            </TapGestureHandler>
        </View>
    )
}
GrowingBall
    .displayName = 'GrowingBall' +
    ''

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    box: {
        borderRadius: 50,
        width: 100,
        height: 100,
        backgroundColor: 'coral',
        marginBottom: 100,
    }
})

export default GrowingBall

