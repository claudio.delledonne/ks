import * as React from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
  UIManager,
  Animated,
  Easing,
  TextInput,
} from "react-native";

function StaggerAnimateScreen() {
  let animatedValues = [];
  for (let i = 0; i < 1000; i++) {
    animatedValues[i] = new Animated.Value(0);
  }
  const animations = animatedValues.map((value) =>
    Animated.timing(value, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
      easing: Easing.linear,
    })
  );
  React.useEffect(() => {
    Animated.stagger(15, animations).start();
  }, []);
  return (
    <View
      style={{
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
      }}
    >
      {animatedValues.map((value, index) => {
        return (
          <Animated.View
            key={index}
            style={[styles.box, { opacity: value }]}
          ></Animated.View>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  box: {
    margin: 0.5,
    width: 15,
    height: 15,
    backgroundColor: "red",
  },
});

export default StaggerAnimateScreen;
