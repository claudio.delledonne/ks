import * as React from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
  UIManager,
  Animated,
  Dimensions,
} from "react-native";

if (Platform.OS === "android") {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

function AnimatedBallScreen() {
  const position = React.useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;
  const moveDown = () => {
    Animated.spring(position, {
      toValue: {
        x: Dimensions.get("window").width / 2 - 50,
        y: Dimensions.get("window").height - 250,
      },
      useNativeDriver: true,
    }).start();
  };
  const moveUp = () => {
    Animated.spring(position, {
      toValue: 0,
      useNativeDriver: true,
    }).start();
  };
  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <Button title="Move Down box" onPress={moveDown}></Button>
      <Button title="Move Up box" onPress={moveUp}></Button>
      <Animated.View
        style={[
          styles.ball,
          {
            transform: [{ translateX: position.x }, { translateY: position.y }],
          },
        ]}
      ></Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  ball: {
    width: 100,
    height: 100,
    backgroundColor: "red",
    borderRadius: 50,
  },
});

export default AnimatedBallScreen;
