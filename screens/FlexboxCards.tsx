import * as React from "react";
import {
  View,
  TouchableOpacity,
  ScrollView,
  StyleProp,
  ViewStyle,
} from "react-native";
import Card, { Props as CardProps } from "../components/Card";

const SmallCard = ({ style }: { style?: StyleProp<ViewStyle> }) => {
  return (
    <Card
      title="Antonio Calanducci"
      subtitle="Instructor"
      description="Antonio is a Javascript (and TS) fan and React Native mobile developer"
      image={require("../assets/favicon.png")}
      size={{ width: 200, height: 400 }}
      styleCard={style}
    ></Card>
  );
};

function FlexboxCardsScreen() {
  return (
    <ScrollView>
      <View>
        <SmallCard />
        <SmallCard style={{ alignSelf: "center" }} />
        <SmallCard style={{ alignSelf: "flex-end" }} />
        <SmallCard style={{ alignSelf: "center" }} />
        <SmallCard style={{ alignSelf: "flex-start" }} />
      </View>
    </ScrollView>
  );
}

export default FlexboxCardsScreen;
