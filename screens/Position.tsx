import * as React from "react";
import { View, Text, StyleSheet, Platform } from "react-native";
import Box from "../components/Box";
import CenteredText from "../components/CenteredText";

function PositionScreen() {
  return (
    <View style={styles.container}>
      <Box>
        <CenteredText>A</CenteredText>
      </Box>
      <Box>
        <CenteredText>B</CenteredText>
        <Box style={styles.tinyBox}>
          <CenteredText>E</CenteredText>
        </Box>
      </Box>
      <Box>
        <CenteredText>C</CenteredText>
      </Box>
      <Box style={{ position: "absolute", bottom: 0, right: 0 }}>
        <CenteredText>D</CenteredText>
      </Box>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginTop: Platform.OS === "android" ? 100 : 200,
    width: 360,
    height: 360,
    borderWidth: 2,
    borderColor: "black",
  },
  tinyBox: {
    width: 30,
    height: 30,
    position: "absolute",
    bottom: 0,
    right: 0,
  },
});

export default PositionScreen;
