import * as React from "react";
import {
  View,
  Text,
  Button,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  Pressable,
  StatusBar,
} from "react-native";

function TouchablesScreen() {
  const onPress = () => {
    console.log("on press");
  };
  const onPressIn = () => {
    console.log("on press in");
  };
  const onPressOut = () => {
    console.log("on press out");
  };
  const onLongPress = () => {
    console.log("on long press");
  };
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <StatusBar
        hidden={false}
        backgroundColor="coral"
        translucent={false}
        barStyle="dark-content"
      ></StatusBar>
      <Button title="Cliccami" onPress={onPress} color={"coral"}></Button>
      <TouchableOpacity onPress={onPress} activeOpacity={0.4}>
        <View
          style={{
            marginTop: 15,
            alignItems: "center",
            justifyContent: "center",
            width: 200,
            height: 50,
            backgroundColor: "blue",
          }}
        >
          <Text style={{ color: "white" }}>Touchable opacity</Text>
        </View>
      </TouchableOpacity>
      <View
        style={{
          marginTop: 15,
        }}
      >
        <TouchableHighlight
          underlayColor="black"
          activeOpacity={0.4}
          onPress={onPress}
        >
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              width: 200,
              height: 50,
              backgroundColor: "blue",
            }}
          >
            <Text style={{ color: "white" }}>Touchable highlight</Text>
          </View>
        </TouchableHighlight>
      </View>
      <TouchableNativeFeedback
        onPress={onPress}
        onLongPress={onLongPress}
        background={TouchableNativeFeedback.SelectableBackground(10)}
      >
        <View
          style={{
            marginTop: 15,
            alignItems: "center",
            justifyContent: "center",
            width: 200,
            height: 50,
            backgroundColor: "#2196f3",
          }}
        >
          <Text style={{ color: "white" }}>Touchable native feedback</Text>
        </View>
      </TouchableNativeFeedback>
      <TouchableWithoutFeedback onPress={onPress} onLongPress={onLongPress}>
        <View
          style={{
            marginTop: 15,
            alignItems: "center",
            justifyContent: "center",
            width: 200,
            height: 50,
            backgroundColor: "#2196f3",
          }}
        >
          <Text style={{ color: "white" }}>Touchable without feedback</Text>
        </View>
      </TouchableWithoutFeedback>
      <Pressable
        onPress={onPress}
        onPressIn={onPressIn}
        onPressOut={onPressOut}
        onLongPress={onLongPress}
        hitSlop={15}
        pressRetentionOffset={30}
        style={({ pressed }) => {
          return [
            {
              marginTop: 15,
              alignItems: "center",
              justifyContent: "center",
              width: 200,
              height: 50,
              backgroundColor: pressed ? "red" : "#2196f3",
            },
          ];
        }}
      >
        {({ pressed }) => {
          return (
            <Text style={{ color: "white" }}>
              {pressed ? "pressed" : "press me"}
            </Text>
          );
        }}
      </Pressable>
    </View>
  );
}

export default TouchablesScreen;
