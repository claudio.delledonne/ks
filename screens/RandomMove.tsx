import React from 'react'
import {Button, StyleSheet, Text, View} from 'react-native'
import Animated, {useAnimatedStyle, useSharedValue, withSpring} from 'react-native-reanimated'

const RandomMove= (): JSX.Element => {
    const offset = useSharedValue(10)
    const box1AnimatedStyle = useAnimatedStyle(() => {
        return {
            transform: [{translateX: withSpring(offset.value, {
                damping: 20,
                stiffness: 90,
                velocity: 2
                })
            }]
        }
    })
    const box2AnimatedStyle = useAnimatedStyle(() => {
        return {
            transform: [{translateX: withSpring(offset.value, {
                    damping: 20,
                    stiffness: 90,
                    velocity: 2
                })
            }]
        }
    })
    const changeOffset = () => {
        offset.value = Math.random()*200
    }
    return (
        <View style={styles.container}>
            <Animated.View style={[styles.box, box1AnimatedStyle]}/>
            <Animated.View style={[styles.box, {backgroundColor: 'red'}, box2AnimatedStyle]}/>
            <Button onPress={changeOffset} title={'Change offset'}/>
        </View>
    )
}
RandomMove.displayName = 'RandomMove'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    box: {
        marginVertical: 10,
        width: 100,
        height: 100,
        borderRadius: 4,
        backgroundColor: 'coral'
    }
})

export default RandomMove
