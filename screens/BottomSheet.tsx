import React from 'react'
import {Button, Dimensions, StyleSheet, Text, View} from 'react-native'
import Animated, {
    useAnimatedGestureHandler,
    useAnimatedStyle,
    useSharedValue,
    withSpring
} from 'react-native-reanimated'
import {
    PanGestureHandler,
    PanGestureHandlerGestureEvent,
    TapGestureHandler,
    TapGestureHandlerGestureEvent
} from "react-native-gesture-handler";

type TContext = { top: number }

const SCREEN_HEIGHT = Dimensions.get('window').height

const BottomSheet
    = (): JSX.Element => {
    const top = useSharedValue(SCREEN_HEIGHT)
    const animatedStyle = useAnimatedStyle(() => {
        return {
            top: withSpring(top.value, {velocity: 2})
        }
    })
    const gestureHandler = useAnimatedGestureHandler<PanGestureHandlerGestureEvent, TContext>({
        onStart: (event, context) => {
            context.top = top.value
        },
        onEnd: (event, context) => {
            if (top.value > SCREEN_HEIGHT / 2) {
                top.value = SCREEN_HEIGHT
            } else {
                top.value = 0
            }
        },
        onActive: (event, context) => {
            top.value = event.translationY + context.top
        }
    })
    const onPress = () => {
        top.value = SCREEN_HEIGHT / 2
    }
    return (
        <>
            <View style={styles.container}><Button title={'show'} onPress={onPress}/></View>
            <PanGestureHandler onGestureEvent={gestureHandler}>
                <Animated.View style={[styles.sheet, animatedStyle]}><Text>Sheet</Text></Animated.View>
            </PanGestureHandler>
        </>

    )
}
BottomSheet
    .displayName = 'BottomSheet' +
    ''

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    sheet: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'white',
        padding: 30,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        elevation: 5,
    }
})

export default BottomSheet

