import * as React from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
  UIManager,
  Animated,
  Easing,
} from "react-native";

if (Platform.OS === "android") {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

function BoxAnimateScreen() {
  const marginTop = React.useRef(new Animated.Value(0)).current;
  const marginTop2 = React.useRef(new Animated.Value(200)).current;
  const moveDown = () => {
    Animated.timing(marginTop, {
      toValue: 100,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.inOut(Easing.ease),
    }).start();
  };
  const moveUp = () => {
    Animated.timing(marginTop, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.inOut(Easing.ease),
    }).start();
  };
  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <Button title="Move Down box" onPress={moveDown}></Button>
      <Button title="Move Up box" onPress={moveUp}></Button>
      <Animated.View
        style={[
          styles.box,
          {
            transform: [
              {
                translateY: marginTop,
              },
            ],
          },
        ]}
      ></Animated.View>

      {/* <Animated.View
        style={[
          styles.box,
          {
            backgroundColor: "blue",
            transform: [
              {
                translateY: marginTop2,
              },
            ],
          },
        ]}
      ></Animated.View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: "red",
  },
});

export default BoxAnimateScreen;
