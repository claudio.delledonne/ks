import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Box from '../components/Box';
import CenteredText from '../components/CenteredText';

function BorderShapesScreen() {
    return (
      <View style={styles.container}>
        <View style={styles.row}>
            <Box style={{borderRadius: 20}}><CenteredText>A</CenteredText></Box>
            <Box style={{borderTopRightRadius: 60, borderBottomRightRadius: 60}}><CenteredText>B</CenteredText></Box>
        </View>
        <View style={styles.row}>
            <Box style={{borderTopLeftRadius: 20, borderBottomRightRadius: 20}}><CenteredText>C</CenteredText></Box>
            <Box style={{borderRadius: 60}}><CenteredText>D</CenteredText></Box>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        margin: 10,
    }
});

export default BorderShapesScreen;