import React from 'react'
import {Button, StyleSheet, Text, View} from 'react-native'
import Animated, {
    interpolateColor,
    useAnimatedGestureHandler, useAnimatedScrollHandler,
    useAnimatedStyle,
    useSharedValue,
} from 'react-native-reanimated'
import {
    PanGestureHandler,
    PanGestureHandlerGestureEvent,
    TapGestureHandler,
    TapGestureHandlerGestureEvent
} from "react-native-gesture-handler";

type TContext = { offset: { x: number, y: number } }
const ScrollAnimation
    = (): JSX.Element => {
    const pressed = useSharedValue(false)
    const x = useSharedValue(0)
    const y = useSharedValue(0)
    const scrollY = useSharedValue(0)
    const boxAnimatedStyle = useAnimatedStyle(() => {
        return {
            backgroundColor: pressed.value ? 'red' : 'coral',
            transform: [
                /*
                                {scale: withSpring(pressed.value ? 1.5 : 1, {velocity: 2})},
                */
                {translateX: x.value},
                {translateY: y.value},
            ]
        }
    })
    const gestureHandler = useAnimatedGestureHandler<PanGestureHandlerGestureEvent, TContext>({
        onStart: (event, context) => {
            pressed.value = true
            context.offset = { x: x.value, y: y.value }
        },
        onEnd: (event, context) => {
            pressed.value = false
        },
        onActive: (event, context) => {
            x.value = event.translationX + context.offset.x
            y.value = event.translationY + context.offset.y
        }
    })
    const onScroll = useAnimatedScrollHandler({
        onScroll: (event, context) => {
            scrollY.value = event.contentOffset.y
        }
    })
    const scrollViewAnimatedStyle = useAnimatedStyle(() => {
        return {
            backgroundColor: interpolateColor(
                scrollY.value,
                [-50, 0, 50],
                ["rgb(10,10,10)", "rgb(255,255,255)", "rgb(10,10,10)"],
                'RGB'
            ) as string,
        }
    })
    return (
        <Animated.ScrollView scrollEventThrottle={16} style={scrollViewAnimatedStyle} contentContainerStyle={[styles.container]} onScroll={onScroll}>
            <PanGestureHandler onGestureEvent={gestureHandler}>
                <Animated.View style={[styles.box, boxAnimatedStyle]}/>
            </PanGestureHandler>
        </Animated.ScrollView>
    )
}
ScrollAnimation
    .displayName = 'ScrollAnimation' +
    ''

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    box: {
        borderRadius: 50,
        width: 100,
        height: 100,
        backgroundColor: 'coral',
        marginBottom: 100,
    }
})

export default ScrollAnimation

