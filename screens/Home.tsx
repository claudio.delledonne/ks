import * as React from "react";
import { View, Text, Button, StyleSheet, TouchableOpacity } from "react-native";
import { StackScreenProps } from "@react-navigation/stack";
import { RootStackParamList } from "../types";
import { screenList } from "../screenList";
import { FlatList } from "react-native-gesture-handler";
import { Entypo } from "@expo/vector-icons";

type Props = StackScreenProps<RootStackParamList, "Home">;

function HomeScreen({ navigation }: Props) {
  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <Text>Home Screen</Text>
      <FlatList
        data={screenList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View style={styles.row}>
            <TouchableOpacity onPress={() => navigation.navigate(item.name)}>
              <View style={styles.rowContent}>
                <Text style={{ flex: 1 }}>{item.title}</Text>
                <Entypo name="chevron-right" size={24} color="black"></Entypo>
              </View>
            </TouchableOpacity>
          </View>
        )}
      />
      {/* <Button
        title="Go to Details Screen"
        onPress={() => navigation.navigate("Details")}
      ></Button>
      <Button
        title="Go to Simple Theme Screen"
        onPress={() => navigation.navigate("SimpleTheme")}
      ></Button>
      <Button
        title="Go to Border Shapes Screen"
        onPress={() => navigation.navigate("BorderShapes")}
      ></Button>
      <Button
        title="Go to Position Screen"
        onPress={() => navigation.navigate("Position")}
      ></Button>
      <Button
        title="Go to My Profile Screen"
        onPress={() => navigation.navigate("MyProfile")}
      ></Button>
      <Button
        title="Go to Transforms example"
        onPress={() => navigation.navigate("Transforms")}
      ></Button>
      <Button
        title="Go to Flexbox example"
        onPress={() => navigation.navigate("Flexbox")}
      ></Button>
      <Button
        title="Go to FlexboxCard example"
        onPress={() => navigation.navigate("FlexboxCards")}
      ></Button>
      <Button
        title="Go to List example"
        onPress={() => navigation.navigate("List")}
      ></Button> */}
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    height: 50,
    justifyContent: "center",
    paddingHorizontal: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "gray",
    marginLeft: 10,
  },
  rowContent: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default HomeScreen;
