import * as React from "react";
import { useState } from "react";
import { View, Text } from "react-native";
import { FlatList, TextInput } from "react-native-gesture-handler";
import { List } from "./List";

function ToDoListScreen() {
  const [value, setValue] = useState("");
  const [toDoList, setToDoList] = useState<string[]>([]);
  const data = toDoList.map((item, index) => {
    return {
      id: index,
      nome: item,
    };
  });
  return (
    <View style={{ flex: 1 }}>
      <TextInput
        placeholder={"Cosa c'è da fare?"}
        style={{
          height: 50,
          padding: 10,
        }}
        value={value}
        onChangeText={(text) => setValue(text)}
        onSubmitEditing={() => {
          setToDoList([...toDoList, value]);
          setValue("");
        }}
        autoFocus
        blurOnSubmit={false}
      ></TextInput>
      <Text>{value}</Text>
      <List data={data}></List>
    </View>
  );
}

export default ToDoListScreen;
