import * as React from "react";
import { View, TouchableOpacity } from "react-native";
import Card from "../components/Card";

function MyProfileScreen() {
  const [showThumbnail, setShowThumbnail] = React.useState(false);
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        margin: 30,
      }}
    >
      <TouchableOpacity onPress={() => setShowThumbnail(!showThumbnail)}>
        <Card
          title="Antonio Calanducci"
          subtitle="Instructor"
          description="Antonio is a Javascript (and TS) fan and React Native mobile developer"
          image={require("../assets/favicon.png")}
          showThumbnail={showThumbnail}
          size={{ width: 200, height: 400 }}
        ></Card>
      </TouchableOpacity>
    </View>
  );
}

export default MyProfileScreen;
