import * as React from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
  UIManager,
  Animated,
  Easing,
  TextInput,
} from "react-native";

function LoadingAnimateScreen() {
  const [loading, setLoading] = React.useState(true);
  const rotation = React.useRef(new Animated.Value(0)).current;
  const rotationInterpolation = rotation.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"],
  });
  const animateImage = () => {
    Animated.loop(
      Animated.timing(rotation, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
        easing: Easing.linear,
      })
    ).start();
  };
  React.useEffect(() => {
    animateImage();
    setTimeout(() => setLoading(false), 4000);
  }, []);
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      {loading ? (
        <Animated.Image
          source={require("../assets/favicon.png")}
          style={{
            width: 80,
            height: 80,
            transform: [{ rotate: rotationInterpolation }],
          }}
        ></Animated.Image>
      ) : (
        <Text>Welcome</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({});

export default LoadingAnimateScreen;
