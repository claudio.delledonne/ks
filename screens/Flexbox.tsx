import * as React from "react";
import { View, Text, StyleSheet, Platform } from "react-native";
import Box, { BoxProps } from "../components/Box";
import CenteredText from "../components/CenteredText";

const Box100 = (props: BoxProps) => (
  <Box {...props} style={[props.style, { alignItems: "center" }]}></Box>
);

function FlexboxScreen() {
  return (
    <View style={styles.container3}>
      <Box100>
        <Text>A</Text>
      </Box100>
      <Box100 style={styles.box1} color="orange">
        <Text>B</Text>
      </Box100>
      <Box100 style={styles.box2} color="purple">
        <Text>C</Text>
      </Box100>
      <Box100 style={styles.box3} color="red">
        <Text>D</Text>
      </Box100>
      <Box100 style={styles.box4} color="blue">
        <Text>E</Text>
      </Box100>
      <Box100 style={styles.box5} color="black">
        <Text>F</Text>
      </Box100>
    </View>
  );
}

const styles = StyleSheet.create({
  container1: {
    flex: 1,
    flexDirection: "row",
    // justifyContent: "flex-start",
    // alignItems: "flex-start",
  },
  container2: {
    flex: 1,
    // flexDirection: "column",
    justifyContent: "space-between",
    // alignItems: "flex-start",
  },
  container3: {
    flex: 1,
    // flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    // width: 340,
    // backgroundColor: "grey",
  },
  box1: {
    flex: 2,
  },
  box2: {
    flex: 1,
    alignSelf: "flex-end",
  },
  box3: {
    flex: 4,
  },
  box4: {
    flex: 2,
    alignSelf: "flex-end",
  },
  box5: {
    flex: 2,
    alignSelf: "flex-start",
  },
});

export default FlexboxScreen;
