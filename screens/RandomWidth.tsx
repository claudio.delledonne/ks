import React from 'react'
import {Button, StyleSheet, Text, View} from 'react-native'
import Animated, {useAnimatedStyle, useSharedValue, withSpring} from 'react-native-reanimated'

const RandomWidth= (): JSX.Element => {
    const width = useSharedValue(100)
    const boxAnimatedStyle = useAnimatedStyle(() => {
        return { width: withSpring(width.value, {velocity: 2}) }
    })
    const changeWidth = () => {
        width.value = Math.random()*200
    }
    return (
        <View style={styles.container}>
            <Animated.View style={[styles.box, boxAnimatedStyle]}/>
            <Button onPress={changeWidth} title={'Change width'}/>
        </View>
    )
}
RandomWidth.displayName = 'RandomWidth'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    box: {
        height: 100,
        backgroundColor: 'coral'
    }
})

export default RandomWidth
