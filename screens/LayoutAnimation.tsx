import * as React from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  LayoutAnimationConfig,
  Alert,
} from "react-native";

if (Platform.OS === "android") {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

function LayoutAnimationScreen() {
  const [boxPosition, setBoxPosition] = React.useState("left");
  const [color, setColor] = React.useState("blue");
  const onButtonPress = () => {
    const configAnimation = LayoutAnimation.create(
      300,
      LayoutAnimation.Types.easeIn,
      LayoutAnimation.Properties.opacity
    );
    const configAnimation2: LayoutAnimationConfig = {
      duration: 300,
      create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.scaleXY,
      },
      delete: {
        type: LayoutAnimation.Types.easeOut,
        property: LayoutAnimation.Properties.opacity,
        duration: 1000,
      },
      update: {
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 0.5,
      },
    };
    const onAnimationDidEnd = () => {
      setColor(color === "blue" ? "red" : "blue");
    };
    LayoutAnimation.configureNext(configAnimation2, onAnimationDidEnd);
    setBoxPosition(boxPosition === "left" ? "right" : "left");
  };
  return (
    <View style={{ flex: 1, justifyContent: "space-around" }}>
      {boxPosition === "right" && (
        <Text style={styles.text}>Welcome Layout Animation</Text>
      )}
      <Button title="Toggle box" onPress={onButtonPress}></Button>
      <View
        style={[
          styles.box,
          boxPosition === "right" ? styles.boxPosition : null,
          { backgroundColor: color },
        ]}
      ></View>
    </View>
  );
}

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: "black",
    margin: 10,
    borderRadius: 5,
  },
  boxPosition: {
    width: 200,
    height: 200,
    alignSelf: "flex-end",
  },
  text: {
    fontSize: 48,
    fontWeight: "bold",
    alignSelf: "center",
    margin: 10,
  },
});

export default LayoutAnimationScreen;
