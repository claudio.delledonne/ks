import * as React from "react";
import { View, Text, StyleSheet, Platform } from "react-native";
import Box from "../components/Box";
import CenteredText from "../components/CenteredText";
import Slider from "@react-native-community/slider";

type TSliderProps = {
  min: number;
  max: number;
  onChange: (v: number) => void;
  starting?: number;
  step?: number;
};

const TSlider = ({
  min,
  max,
  onChange,
  starting = 0,
  step = 1,
}: TSliderProps) => (
  <Slider
    minimumValue={min}
    maximumValue={max}
    value={starting}
    step={step}
    style={{ width: "60%", marginHorizontal: 20 }}
    onValueChange={onChange}
  />
);

function TransformsScreen() {
  const [value, setValue] = React.useState({
    r: 0,
    rX: 0,
    rY: 0,
    s: 1,
    sX: 0,
    sY: 0,
    tX: 0,
    tY: 0,
  });
  return (
    <View style={styles.container}>
      <Box
        style={{
          transform: [
            { rotate: `${value.r}deg` },
            { rotateX: `${value.rX}deg` },
            { scale: value.s },
          ],
        }}
      >
        <CenteredText>A</CenteredText>
      </Box>
      <View style={styles.sliderContainer}>
        <View style={styles.row}>
          <Text>Rotate</Text>
          <TSlider
            min={0}
            max={360}
            onChange={(v) => setValue({ ...value, r: v })}
          />
          <Text style={{ width: 50 }}>{`${value.r} °`}</Text>
        </View>
        <View style={styles.row}>
          <Text>Rotate X</Text>
          <TSlider
            min={0}
            max={360}
            onChange={(v) => setValue({ ...value, rX: v })}
          />
          <Text style={{ width: 50 }}>{`${value.rX} °`}</Text>
        </View>
        <View style={styles.row}>
          <Text>Scale</Text>
          <TSlider
            starting={1}
            min={0.1}
            max={3}
            step={0.1}
            onChange={(v) => setValue({ ...value, s: Number(v.toFixed(2)) })}
          />
          <Text style={{ width: 50 }}>{`${value.s}`}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flexDirection: "row",
    marginTop: Platform.OS === "android" ? 100 : 100,
    flex: 1,
    alignItems: "center",
    // width: 360,
    // height: 360,
    // borderWidth: 2,
    // borderColor: "black",
  },
  sliderContainer: {
    marginTop: 30,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  row: {
    width: "95%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});

export default TransformsScreen;
