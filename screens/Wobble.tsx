import React from 'react'
import {Button, StyleSheet, Text, View} from 'react-native'
import Animated, {
    useAnimatedStyle,
    useSharedValue,
    withRepeat,
    withSequence,
    withSpring,
    withTiming
} from 'react-native-reanimated'

const Wobble= (): JSX.Element => {
    const rotateZ = useSharedValue(0)
    const box1AnimatedStyle = useAnimatedStyle(() => {
        return {
            transform: [{rotateZ: `${rotateZ.value}deg`}]
        }
    })
    const changeStyle = () => {
        rotateZ.value = withSequence(
            withTiming(-10),
            withRepeat(withTiming(45), -1, true),
            withTiming(0)
        )
    }
    return (
        <View style={styles.container}>
            <Animated.View style={[styles.box, box1AnimatedStyle]}/>
            <Button onPress={changeStyle} title={'Change style'}/>
        </View>
    )
}
Wobble.displayName = 'Wobble'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    box: {
        marginVertical: 10,
        width: 100,
        height: 100,
        borderRadius: 4,
        backgroundColor: 'coral'
    }
})

export default Wobble
