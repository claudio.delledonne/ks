import * as React from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  PanResponder,
  GestureResponderEvent,
  PanResponderGestureState,
  Animated,
} from "react-native";

const PanResponderAnimatedScreen = () => {
  const pan = React.useRef(new Animated.ValueXY()).current;
  const offset = React.useRef({ x: 0, y: 0 }).current;

  const handlePanResponderMove = (
    evt: GestureResponderEvent,
    gestureState: PanResponderGestureState
  ) => {
    pan.setValue({
      x: offset.x + gestureState.dx,
      y: offset.y + gestureState.dy,
    });
  };

  const handlePanResponderRelease = (
    evt: GestureResponderEvent,
    gestureState: PanResponderGestureState
  ) => {
    offset.x += gestureState.dx;
    offset.y += gestureState.dy;
  };

  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => console.log(gestureState),
      onPanResponderMove: handlePanResponderMove,
      onPanResponderRelease: handlePanResponderRelease,
    })
  ).current;

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      {/* <Text style={styles.text}>
        x: {position.x} y: {position.y}
      </Text> */}
      <Animated.View
        {...panResponder.panHandlers}
        style={[
          styles.box,
          {
            transform: [{ translateX: pan.x }, { translateY: pan.y }],
          },
        ]}
      ></Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    // position: "absolute",
    textAlign: "center",
    marginTop: 50,
  },
  box: {
    // position: "absolute",
    height: 200,
    width: 200,
    backgroundColor: "red",
    borderRadius: 5,
  },
});

export default PanResponderAnimatedScreen;
