import * as React from "react";
import {
  View,
  TouchableOpacity,
  Dimensions,
  Button,
  Modal,
  StatusBar,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Card from "../components/Card";

const ProfileCard = () => (
  <Card
    styleCard={{ marginHorizontal: 10, marginVertical: 10 }}
    title="Antonio Calanducci"
    subtitle="Instructor"
    description="Antonio is a Javascript (and TS) fan and React Native mobile developer"
    image={require("../assets/favicon.png")}
    size={{ width: Dimensions.get("window").width * 0.8, height: 400 }}
  ></Card>
);

function ScrollViewScreen() {
  const _scrollViewRef = React.useRef<ScrollView>(null);
  const [modalVisible, setModalVisible] = React.useState(false);
  return (
    <View style={{ alignItems: "center", justifyContent: "center" }}>
      <StatusBar
        hidden={false}
        backgroundColor="transparent"
        translucent={true}
        barStyle="dark-content"
      ></StatusBar>
      <Modal
        transparent
        animationType={"slide"}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <View
          style={{
            width: 300,
            height: 400,
            backgroundColor: "red",
          }}
        >
          <Button
            title="Close modal"
            onPress={() => setModalVisible(false)}
          ></Button>
        </View>
      </Modal>
      <ScrollView
        ref={_scrollViewRef}
        horizontal
        pagingEnabled
        snapToInterval={Dimensions.get("window").width * 0.8 + 20}
      >
        <ProfileCard />
        <ProfileCard />
        <ProfileCard />
        <ProfileCard />
      </ScrollView>
      <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
        <Button
          title="Go to start"
          onPress={() => {
            _scrollViewRef.current?.scrollTo({ x: 0 });
          }}
        ></Button>
        <Button
          title="Go to end"
          onPress={() => {
            _scrollViewRef.current?.scrollToEnd();
          }}
        ></Button>
        <Button
          title="Open modal"
          onPress={() => setModalVisible(true)}
        ></Button>
      </View>
    </View>
  );
}

export default ScrollViewScreen;
