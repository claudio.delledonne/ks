import * as React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

const DATA = [
  {
    id: 1,
    nome: "Antonio",
  },
  {
    id: 2,
    nome: "Luca",
  },
  {
    id: 3,
    nome: "Diego",
  },
  {
    id: 4,
    nome: "Enrico",
  },
  {
    id: 5,
    nome: "Claudio",
  },
  {
    id: 6,
    nome: "Nicola",
  },
  {
    id: 7,
    nome: "Simone",
  },
];

type Item = {
  id: number;
  nome: string;
};

const ListScreen = () => <List data={DATA}></List>;

export function List({ data }: { data: Item[] }) {
  const renderItem = ({ item }: { item: Item }) => (
    <View style={styles.item}>
      <Text style={styles.text}>{item.nome}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        keyExtractor={(item) => item.id.toString()}
        renderItem={renderItem}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: "black",
  },
  item: {
    height: 50,
    justifyContent: "center",
    marginHorizontal: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "gray",
  },
  text: {},
});

export default ListScreen;
