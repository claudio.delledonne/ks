import * as React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextStyle,
  ImageStyle,
  ImageSourcePropType,
  Platform,
} from "react-native";

export type Props = {
  title: string;
  subtitle: string;
  description: string;
  image: ImageSourcePropType;
  size?: { width: number; height: number };
  styleCard?: StyleProp<ViewStyle>;
  styleImageContainer?: StyleProp<ViewStyle>;
  styleImage?: StyleProp<ImageStyle>;
  styleTitle?: StyleProp<TextStyle>;
  styleSubtitleContainer?: StyleProp<ViewStyle>;
  styleSubtitle?: StyleProp<TextStyle>;
  styleDescription?: StyleProp<TextStyle>;
  showThumbnail?: boolean;
};

const defaultSize = {
  width: 300,
  height: 400,
};

function Card({
  title,
  subtitle,
  description,
  image,
  styleCard,
  styleImageContainer,
  styleImage,
  styleTitle,
  styleSubtitleContainer,
  styleSubtitle,
  styleDescription,
  showThumbnail,
  size = defaultSize,
}: Props) {
  let cardStyle = [
    styles.card,
    { width: size.width, height: size.height },
    styleCard,
  ];
  if (showThumbnail) {
    cardStyle.push({ transform: [{ scale: 0.2 }] });
  }
  return (
    <View style={cardStyle}>
      <View style={[styles.imageContainer, styleImageContainer]}>
        <Image source={image} style={[styles.image, styleImage]} />
      </View>
      <Text style={[styles.title, styleTitle]}>{title}</Text>
      <View style={[styles.subtitleContainer, styleSubtitleContainer]}>
        <Text style={[styles.subtitle, styleSubtitle]}>{subtitle}</Text>
      </View>
      {size.width < defaultSize.width ? null : (
        <Text style={[styles.description, styleDescription]}>
          {description}
        </Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    ...Platform.select({
      ios: {
        backgroundColor: "dodgerblue",
        borderWidth: 3,
        borderColor: "black",
      },
      android: {
        backgroundColor: "#449d81",
        borderWidth: 3,
        borderColor: "#832828",
      },
    }),
    ...Platform.select({
      ios: {
        shadowOffset: { width: 20, height: 20 },
        shadowColor: "black",
        shadowOpacity: 0.5,
        shadowRadius: 5,
      },
      android: {
        elevation: 4,
      },
    }),
    // backgroundColor: "dodgerblue",
    // borderWidth: 3,
    // borderColor: "black",
    alignItems: "center",
    paddingTop: 30,
    paddingBottom: 60,
    paddingHorizontal: 30,
    borderRadius: 15,
  },
  imageContainer: {
    backgroundColor: "white",
    padding: 15,
    borderWidth: 3,
    borderColor: "black",
    borderRadius: 55,
    marginBottom: 30,
    ...Platform.select({
      ios: {
        shadowOffset: { width: 20, height: 20 },
        shadowColor: "black",
        shadowOpacity: 0.5,
        shadowRadius: 5,
      },
      android: {
        elevation: 4,
      },
    }),
  },
  image: {
    width: 80,
    height: 80,
  },
  title: {
    color: "white",
    fontSize: 30,
    fontWeight: "bold",
    textShadowColor: "rgba(0, 0, 0, 0.5)",
    textShadowOffset: { width: 2, height: 3 },
    textShadowRadius: 1,
    marginBottom: 15,
    textAlign: "center",
  },
  subtitleContainer: {
    borderBottomWidth: 3,
    borderBottomColor: "black",
    marginBottom: 20,
  },
  subtitle: {
    color: "black",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 5,
  },
  description: {
    fontStyle: "italic",
    fontSize: 16,
  },
});

export default Card;
