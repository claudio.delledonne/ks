import * as React from 'react';
import { Text, StyleSheet, StyleProp, TextStyle } from 'react-native';

type Props = {
    children: string;
    style?: StyleProp<TextStyle>;
};

function CenteredText({children, style}: Props) {
    return (
        <Text style={[styles.text, style]}>{children}</Text>
    );
}

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
        fontWeight: '400',
        margin: 10
    }
});

export default CenteredText;