import * as React from "react";
import { View, StyleSheet, StyleProp, ViewStyle } from "react-native";

export type BoxProps = {
  children?: JSX.Element | Array<JSX.Element>;
  size?: number;
  color?: string;
  style?: StyleProp<ViewStyle>;
};

const defaultSize = 100;

function Box({ children, style, size = defaultSize, color }: BoxProps) {
  return (
    <View
      style={[
        styles.box,
        { width: size, height: size, backgroundColor: color },
        style,
      ]}
    >
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  box: {
    backgroundColor: "gray",
    borderWidth: 2,
    borderColor: "red",
    justifyContent: "center",
  },
});

export default Box;
