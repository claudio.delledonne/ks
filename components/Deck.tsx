import * as React from "react";
import {
  Animated,
  View,
  StyleSheet,
  PanResponder,
  Dimensions,
} from "react-native";
import { Item } from "../screens/SwipableDeck";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SWIPE_THRESHOLD = SCREEN_WIDTH * 0.25;

type Props = {
  data: Item[];
  renderCard: (item: Item) => JSX.Element;
  onSwipeLeft?: (item: Item) => void;
  onSwipeRight?: (item: Item) => void;
};

const Deck = ({ data, renderCard, onSwipeLeft, onSwipeRight }: Props) => {
  const [index, setIndex] = React.useState(0);
  const position = React.useRef(new Animated.ValueXY()).current;

  const onSwipeComplete = (direction: string) => {
    const item = data[index];
    direction === "right" ? onSwipeRight?.(item) : onSwipeLeft?.(item);
    position.setValue({ x: 0, y: 0 });
    setIndex((prevState) => prevState + 1);
  };

  const forceSwipe = (direction: string) => {
    const x = direction === "right" ? SCREEN_WIDTH : -SCREEN_WIDTH;
    Animated.timing(position, {
      toValue: { x, y: 0 },
      duration: 300,
      useNativeDriver: true,
    }).start(() => onSwipeComplete(direction));
  };

  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (evt, gesture) => {
        position.setValue({ x: gesture.dx, y: gesture.dy });
      },
      onPanResponderRelease: (evt, gesture) => {
        if (gesture.dx > SWIPE_THRESHOLD) {
          forceSwipe("right");
        } else if (gesture.dx < -SWIPE_THRESHOLD) {
          forceSwipe("left");
        } else {
          Animated.spring(position, {
            toValue: { x: 0, y: 0 },
            useNativeDriver: true,
          }).start();
        }
      },
    })
  ).current;

  const rotate = position.x.interpolate({
    inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5],
    outputRange: ["-120deg", "0deg", "120deg"],
  });

  const renderCards = () => {
    return data.map((item, i) => {
      if (i < index) {
        return null;
      }
      if (i === index) {
        return (
          <Animated.View
            key={item.id}
            {...panResponder.panHandlers}
            style={{
              transform: [
                { translateX: position.x },
                { translateY: position.y },
                { rotate },
              ],
            }}
          >
            {renderCard(item)}
          </Animated.View>
        );
      } else {
        return <View key={item.id}>{renderCard(item)}</View>;
      }
    });
  };

  return <View>{renderCards()}</View>;
};

const styles = StyleSheet.create({});

export default Deck;
