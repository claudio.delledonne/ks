import DetailsScreen from "./screens/Details";
import SimpleThemeScreen from "./screens/SimpleTheme/SimpleTheme";
import BorderShapesScreen from "./screens/BorderShapes";
import PositionScreen from "./screens/Position";
import MyProfileScreen from "./screens/MyProfile";
import TransformsScreen from "./screens/Transforms";
import FlexboxScreen from "./screens/Flexbox";
import FlexboxCardsScreen from "./screens/FlexboxCards";
import ListScreen from "./screens/List";
import { RootStackParamList } from "./types";
import ToDoListScreen from "./screens/ToDoList";
import ScrollViewScreen from "./screens/ScrollView";
import TouchablesScreen from "./screens/Touchables";
import LayoutAnimationScreen from "./screens/LayoutAnimation";
import BoxAnimateScreen from "./screens/BoxAnimate";
import TextInputAnimateScreen from "./screens/TextInputAnimate";
import LoadingAnimateScreen from "./screens/LoadingAnimate";
import StaggerAnimateScreen from "./screens/StaggerAnimate";
import PanResponderSimpleScreen from "./screens/PanResponderSimple";
import PanResponderAnimatedScreen from "./screens/PanResponderAnimated";
import AnimatedBallScreen from "./screens/AnimatedBall";
import SwipableDeckScreen from "./screens/SwipableDeck";
import RandomWidthScreen from "./screens/RandomWidth";
import RandomMoveScreen from "./screens/RandomMove";
import WobbleScreen from "./screens/Wobble";
import GrowingBallScreen from "./screens/GrowingBall";
import MovingBallScreen from "./screens/MovingBall";
import BottomSheetScreen from "./screens/BottomSheet";
import ScrollAnimationScreen from "./screens/ScrollAnimation";

type Screen = {
  title: string;
  name: keyof RootStackParamList;
  component: () => JSX.Element;
};

export const screenList: Screen[] = [
  { title: "Detail Screen", name: "Details", component: DetailsScreen },
  {
    title: "Simple Theme Screen",
    name: "SimpleTheme",
    component: SimpleThemeScreen,
  },
  {
    title: "Border Shapes Screen",
    name: "BorderShapes",
    component: BorderShapesScreen,
  },
  {
    title: "My Profile Screen",
    name: "MyProfile",
    component: MyProfileScreen,
  },
  {
    title: "To do list Screen",
    name: "ToDoList",
    component: ToDoListScreen,
  },
  {
    title: "ScrollView Screen",
    name: "ScrollView",
    component: ScrollViewScreen,
  },
  {
    title: "Touchables Screen",
    name: "Touchables",
    component: TouchablesScreen,
  },
  {
    title: "Layout Animation Screen",
    name: "LayoutAnimation",
    component: LayoutAnimationScreen,
  },
  {
    title: "Box Animate Screen",
    name: "BoxAnimate",
    component: BoxAnimateScreen,
  },
  {
    title: "Text Input Animate Screen",
    name: "TextInputAnimate",
    component: TextInputAnimateScreen,
  },
  {
    title: "Loading Animate Screen",
    name: "LoadingAnimate",
    component: LoadingAnimateScreen,
  },
  {
    title: "Stagger Animate Screen",
    name: "StaggerAnimate",
    component: StaggerAnimateScreen,
  },
  {
    title: "PanResponder Simple Screen",
    name: "PanResponderSimple",
    component: PanResponderSimpleScreen,
  },
  {
    title: "PanResponder Animated Screen",
    name: "PanResponderAnimated",
    component: PanResponderAnimatedScreen,
  },
  {
    title: "Animated Ball Screen",
    name: "AnimatedBall",
    component: AnimatedBallScreen,
  },
  {
    title: "Swipable Deck Screen",
    name: "SwipableDeck",
    component: SwipableDeckScreen,
  },
  {
    title: "Random Width Screen",
    name: "RandomWidth",
    component: RandomWidthScreen,
  },
  {
    title: "Random Move Screen",
    name: "RandomMove",
    component: RandomMoveScreen,
  },
  {
    title: "Wobble Screen",
    name: "Wobble",
    component: WobbleScreen,
  },
  {
    title: "Growing Ball Screen",
    name: "GrowingBall",
    component: GrowingBallScreen,
  },
  {
    title: "Moving Ball Screen",
    name: "MovingBall",
    component: MovingBallScreen,
  },
  {
    title: "Bottom Sheet Screen",
    name: "BottomSheet",
    component: BottomSheetScreen,
  },
  {
    title: "Scroll Animation Screen",
    name: "ScrollAnimation",
    component: ScrollAnimationScreen,
  },
];
