// In App.js in a new project

import * as React from "react";
import { View, Text, Dimensions, PixelRatio } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { RootStackParamList } from "./types";
import HomeScreen from "./screens/Home";
import { screenList } from "./screenList";

const Stack = createStackNavigator<RootStackParamList>();

function App() {
  const d = Dimensions.get("window");
  console.log(d);
  console.log(PixelRatio.get());
  return (
    <NavigationContainer>
      {/* <Stack.Navigator headerMode={"none"}> */}
      <Stack.Navigator>
        <Stack.Screen key={0} name="Home" component={HomeScreen} />
        {screenList.map((screen, index) => (
          <Stack.Screen
            key={index + 1}
            options={{ title: screen.title }}
            name={screen.name}
            component={screen.component}
          />
        ))}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
